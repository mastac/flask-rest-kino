#!/usr/bin/env python
# -*- coding: utf8 -*-

import argparse

try:
    import queue as Queue
except ImportError:
    import Queue

from sys import argv
from threading import Thread, Event, Lock
import logging
from time import time, sleep
import random
import re

try:
    import urllib.request as urllib2
except ImportError:
    import urllib2

import shelve
import chardet
from datetime import datetime
from kinotables import Actor, Film, Role, Genre, Film_Genre, Foto_Actor, Foto_Film

logging.basicConfig(level=logging.DEBUG,
                    format='(%(asctime)s %(threadName)-15s) %(levelname)s: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')

logger = logging.getLogger('grabber')

# Regex / actors
cbres = re.compile('<b>Краткая биография:</b><br>(.*?)<BR></div>', re.MULTILINE | re.DOTALL)
cares = re.compile('<b>Актерские работы:</b><br>(.*?)(?:<br><br>|<br><script)', re.MULTILINE | re.DOTALL)
crres = re.compile('<br><b>Режисcерские работы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
csres = re.compile('<br><b>Написал сценарии:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
cpres = re.compile('<br><b>Продюсировал фильмы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
cipres = re.compile('<br><b>Исполнительный продюсер:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
cmres = re.compile('<br><br><b>Монтаж:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
cores = re.compile('<br><br><b>Операторские работы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
ckres = re.compile('<br><br><b>Композитор:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
cimg1 = re.compile('img src="foto/(.*?)\.jpg', re.MULTILINE | re.DOTALL)

# Find other types - to actor
typeres  = re.compile('<br><b>(.*?):</b><br>(.*?)<br>', re.MULTILINE | re.DOTALL)

# Regex / films
cbfres = re.compile('<b>Краткое содержание:</b><br>(.*?)<BR></div>', re.MULTILINE | re.DOTALL)

# Regex / common - images
cimgs = re.compile('fotorate\.asp\?img=(.*?)\.jpg', re.MULTILINE | re.DOTALL)
coboi = re.compile('oboirate\.asp\?img=(.*?)\'', re.MULTILINE | re.DOTALL)

months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября',
          'декабря']

errorCout = 0

def remove_html_markup(s):
    tag = False
    quote = False
    out = ""

    for c in s:
        if c == '<' and not quote:
            tag = True
        elif c == '>' and not quote:
            tag = False
        elif (c == '"' or c == "'") and tag:
            quote = not quote
        elif not tag:
            out = out + c

    return out


def get_page(url):

    global errorCout, logger

    req = urllib2.Request(url)
    try:
        res = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
        logger.critical(str(e.code) + ": Error get content - " + url)
        errorCout = errorCout + 1
        return False
    else:
        html = res.read()
        content = html.decode('windows-1251') #.encode('utf8')

        # encoding = 'windows-1251' #chardet.detect(html)['encoding']
        # if encoding != 'utf-8':
            # content = html.decode(encoding, 'replace').encode('utf8')

        # print(content)
        return content


def get_film_by_id(elem, actor, role):

    # check exists film by id

    film = Film.query.filter_by(external_film_id=elem[1]).first()

    if not film:

        content = get_page('http://www.kinoexpert.ru/index.asp?comm=4&num=' + str(elem[1]))

        if not content:
            # TODO: write error IDs to error.log
            logger.critical("Error get content to film ID: " + str(elem[1]))
            return False

        bfres = cbfres.findall(content)

        # cimgs = re.compile('fotorate\.asp\?img=(.*?)\.jpg', re.MULTILINE | re.DOTALL)
        imgs = cimgs.findall(content)

        # coboi = re.compile('oboirate\.asp\?img=(.*?)\'', re.MULTILINE | re.DOTALL)
        obois = coboi.findall(content)

        post = {}

        post['description'] = ''
        if bfres:
            post['description'] = remove_html_markup(bfres[0])

        post['filmId'] = elem[1]

        lastdate = re.findall('<u>Последнее обновление: (.*?)</u>', content);
        post['update_date'] = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        if lastdate:
            post['update_date'] = datetime.strptime(lastdate[0], "%d.%m.%Y %H:%M:%S")

        post['fotoImage'] = []
        if imgs:
            post['fotoImage'] = imgs

        post['fotoOboi'] = []
        if obois:
            post['fotoOboi'] = obois

        film = Film.get_or_create(elem[3], elem[4], elem[5], elem[1], post['description'], post['update_date'])

        # film images
        for img in post['fotoImage']:
            foto_film = Foto_Film.get_or_create(img, 'image', film.id)

        for img in post['fotoOboi']:
            foto_film = Foto_Film.get_or_create(img, 'oboi', film.id)

    actor.add_films([(film.id, role.id)])

    return film


def parse_film_items(str, role, actor):

    items = []

    if str:
        ritems = re.compile(
            '&nbsp;(\d+)\.\s<a class=l2 href=index\.asp\?comm=4&num=(.*?)(?:(?=>)(.*?)>| title="(.*?)">)(.*?)<\/a> - (.*?) \((.*?)\)',
            re.MULTILINE | re.DOTALL | re.IGNORECASE)

        for elem in ritems.findall(str):

            film = get_film_by_id(elem, actor, role)

            gen_names = []

            if elem[6] != '':
                gen = elem[6]
                gen_list = gen.split('/')

                for item in gen_list:
                    gen_name = " ".join(item.strip().capitalize().split())
                    if gen_name:
                        gen_names.append(gen_name)
                        genre = Genre.get_or_create(gen_name)

                        film_genre = Film_Genre.get_or_create(genre.id, film.id)

            items.append({'num': elem[0],
                          'filmID': elem[1],
                          'filmInfo': film,
                          'original_name': elem[3],
                          'translate_name': elem[4],
                          'year': elem[5],
                          'genres': gen_names})

    # print items
    return items


def get_actor_by_id(id):

    actor = Actor.query.filter_by(external_actor_id=id).first()

    if not actor:

        content = get_page('http://www.kinoexpert.ru/index.asp?comm=5&kw=' + str(id))
        if not content:
            logger.critical("Error get content to actor ID: " + str(id))
            return False

        content = content

        # bres = cbres.findall(content)

        # http://regex101.com/r/oE0gR2
        nres = re.findall('<h1>(.*?)(?:(?=\S/\S)(.*?)| / (.*?))</td></tr></h1>', content)

        # http://regex101.com/r/tK8jP8
        dres = re.findall(
            '<td colspan=3 align=center><font size=2 color=#008000>(\d+)\s(.*?)\s(\d+)(?:(?=\s\-\s)\s\-\s(\d+)\s(.*?)\s(\d+)|(.*?))</font>',
            content)

        # cbres = re.compile('<b>Краткая биография:</b><br>(.*?)<BR></div>', re.MULTILINE | re.DOTALL)
        bres = cbres.findall(content)

        # cares = re.compile('<b>Актерские работы:</b><br>(.*?)(?:<br><br>|<br><script)', re.MULTILINE | re.DOTALL)
        ares = cares.findall(content)

        # crres = re.compile('<br><b>Режисcерские работы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        rres = crres.findall(content)

        # csres = re.compile('<br><b>Написал сценарии:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        sres = csres.findall(content)

        # cpres = re.compile('<br><b>Продюсировал фильмы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        pres = cpres.findall(content)

        # cipres = re.compile('<br><b>Исполнительный продюсер:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        ipres = cipres.findall(content)

        # cmres = re.compile('<br><br><b>Монтаж:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        mres = cmres.findall(content)

        # cores = re.compile('<br><br><b>Операторские работы:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        ores = cores.findall(content)

        # ckres = re.compile('<br><br><b>Композитор:</b><br>(.*?)<br><br>', re.MULTILINE | re.DOTALL)
        kres = ckres.findall(content)

        # cimg1 = re.compile('img src="foto/(.*?)\.jpg', re.MULTILINE | re.DOTALL)
        img_main = cimg1.findall(content)

        # cimgs = re.compile('fotorate\.asp\?img=(.*?)\.jpg', re.MULTILINE | re.DOTALL)
        imgs = cimgs.findall(content)

        # coboi = re.compile('oboirate\.asp\?img=(.*?)\'', re.MULTILINE | re.DOTALL)
        obois = coboi.findall(content)

        lastdate = re.findall('<u>Последнее обновление: (.*?)</u>', content)

        post = {}

        post['id'] = id

        post['name'] = ''
        post['name_eng'] = ''
        if nres:
            post['name'] = nres[0][0]
            if nres[0][2]:
                post['name_eng'] = nres[0][2]

        post['death_day'] = None
        post['birth_date'] = None
        if dres:
            # 15690, 32358, 32358 - problem parse
            if dres[0][0] and dres[0][2] and dres[0][1] in months:
                post['birth_date'] = dres[0][0] + " " + str(months.index(dres[0][1]) + 1) + " " + dres[0][2]
                try:
                    fixdt = datetime(1900, 1, 1)
                    dt = datetime.strptime(dres[0][2] + "-" + str(int(months.index(dres[0][1]) + 1))
                                           + "-" + dres[0][0], "%Y-%m-%d")

                    # post['birth_date'] = datetime(dt.year + (fixdt - dt).days / 365 + 1, dt.month, dt.day).strftime(
                    #     '%Y-%m-%d').replace('1900', str(dt.year))

                    post['birth_date'] = datetime(int(dres[0][2]), int(months.index(dres[0][1]) + 1), int(dres[0][0]))
                except ValueError as err:
                    pass

            if dres[0][3] and dres[0][5] and dres[0][4] in months:
                post['death_day'] = dres[0][3] + " " + str(months.index(dres[0][4]) + 1) + " " + dres[0][5]
                try:
                    post['death_day'] = datetime(int(dres[0][5]), int(months.index(dres[0][4]) + 1), int(dres[0][3]))
                except ValueError:
                    pass

        post['bio'] = ''
        if bres:
            post['bio'] = remove_html_markup(bres[0])

        post['update_date'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if lastdate:
            post['update_date'] = datetime.strptime(lastdate[0], "%d.%m.%Y %H:%M:%S")

        actor = Actor.get_or_create(post['id'], post['name'], post['name_eng'], post['bio'],
                                    post['birth_date'], post['death_day'], post['update_date'])

        if img_main:
            # foto_actor = Foto_Actor(foto_url=img_main, type='main', actor=actor)
            post['fotoMain'] = img_main
            for img in img_main:
                foto_actor = Foto_Actor.get_or_create(img, 'main', actor.id)
                # get_actor_by_id_trans.add(foto_actor)

        post['fotoImage'] = []
        if imgs:
            post['fotoImage'] += imgs
            for img in imgs:
                foto_actor = Foto_Actor.get_or_create(img, 'image', actor.id)

        post['fotoOboi'] = []
        if obois:
            post['fotoOboi'] = obois
            for img in imgs:
                foto_actor = Foto_Actor.get_or_create(img, 'oboi', actor.id)

        if ares:
            # print "\nАктерские работы:"
            role = Role.get_or_create("Актерские работы")
            post['acting_work'] = parse_film_items(ares[0], role, actor)

        if rres:
            # print "\nРежисcерские работы:"
            role = Role.get_or_create("Режисcерские работы")
            post['director_work'] = parse_film_items(rres[0], role, actor)

        if sres:
            # print "\nНаписал сценарии:"
            role = Role.get_or_create("Написал сценарии")
            post['script_work'] = parse_film_items(sres[0], role, actor)

        if pres:
            # print "\nПродюсировал фильмы:"
            role = Role.get_or_create("Продюсировал фильмы")
            post['produced_work'] = parse_film_items(pres[0], role, actor)

        if ipres:
            # print "\nИсполнительный продюсер:"
            role = Role.get_or_create("Исполнительный продюсер")
            post['executive_producer'] = parse_film_items(ipres[0], role, actor)

        if mres:
            # print "\nМонтаж:"
            role = Role.get_or_create("Монтаж")
            post['mounting_work'] = parse_film_items(mres[0], role, actor)

        if ores:
            # print "\nОператорские работы:"
            role = Role.get_or_create("Операторские работы")
            post['operator_work'] = parse_film_items(ores[0], role, actor)

        if kres:
            # print "\nКомпозитор:"
            role = Role.get_or_create("Композитор")
            post['composer'] = parse_film_items(kres[0], role, actor)

        logger.info(str(actor.external_actor_id) + ": " + actor.name)

    else:

        logger.info(str(actor.external_actor_id) + ": " + actor.name + ' - exist')

    return actor


def threads_main(begin_item=1, end_item=37942, thread_count=10):

    exit_event = Event()
    # store_lock = Lock()

    queue10 = Queue.Queue()

    def process():

        while not exit_event.isSet():

            try:
                queue1 = queue10.get(False)
                get_actor_by_id(queue1)
                queue10.task_done()

                # store_lock.acquire()
                # try:
                #     if post['name']:
                #         dbs[str(queue1)] = post
                # finally:
                #     store_lock.release()

            # except Queue.Empty:
            #     pass

            except (Queue.Empty, Exception) as err:
                if not err:
                    # logger.critical("Error with query id: " + str(queue1))
                    print('Err')
                    print(err)
                    print('ErrArg')
                    print(err.args)
                    # print('Exception: %r' % (e,))
                    queue10.put(int(queue1), False)
                    sleep(1)

    def generate():
        for index in range(begin_item, end_item):
            queue10.put(index, False)

        queue10.join()
        exit_event.set()

    def loop():
        threads = [Thread(name='ThreadName' + str(index), target=process) for index in range(1, thread_count)]
        threads.append(Thread(target=generate))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    try:
        loop()
    except Exception as e:
        print('Exception caught %r' % e)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Get information about actors from site http://www.kinoexpert.ru')
    parser.add_argument('-n', '--num', nargs=1, type=int, help='Id to actor')
    parser.add_argument('-r', '--range', nargs=2, type=int, help='Range to grab information from X to Y')

    options = vars(parser.parse_args())

    # 1 - 37942 - all ids
    if options['num'] and options['num'][0]:
        get_actor_by_id(options['num'][0])
    elif options['range'] and options['range'][0] and options['range'][1]:
        threads_main(options['range'][0], options['range'][1])
    else:
        parser.print_help()

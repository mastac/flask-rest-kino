#!/usr/bin/env python
# -*- coding: utf8 -*-

import datetime
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, DateTime, Text, Enum, ForeignKey
from sqlalchemy.orm import scoped_session, sessionmaker, relationship, backref
from sqlalchemy.ext.associationproxy import association_proxy
import uuid

# engine = sqlalchemy.create_engine('sqlite:///:memory:')
engine = sqlalchemy.create_engine('mysql+mysqlconnector://root:@mysql/mydb?charset=utf8')
db_session = scoped_session(sessionmaker(autocommit=True,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


class Actor_Film_Role(Base):
    __tablename__ = 'actor_film_role'
    actor_id = Column(Integer, ForeignKey('actor.id'), primary_key=True)
    film_id = Column(Integer, ForeignKey('film.id'), primary_key=True)
    role_id = Column(Integer, ForeignKey('role.id'), primary_key=True)

    actor = relationship("Actor", backref=backref("actor_film_roles", cascade="all, delete-orphan" ))
    film = relationship("Film", backref=backref("actor_film_roles", cascade="all, delete-orphan" ))
    role = relationship("Role", backref=backref("actor_film_roles", cascade="all, delete-orphan" ))

    @classmethod
    def get_or_create(cls, actor_id, film_id, role_id):
        instance = cls.query.filter_by(actor_id=actor_id, film_id=film_id, role_id=role_id).first()
        if not instance:
            instance = cls(actor_id=actor_id, film_id=film_id, role_id=role_id)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __init__(self, actor_id=None, film_id=None, role_id=None):
        self.actor_id = actor_id
        self.film_id = film_id
        self.role_id = role_id

    def __repr__(self):
        return '<Actor_Film_Role {}>'.format(self.actor.name+" - "+self.film.original_name+" - "+self.role.name)


class Actor(Base):
    __tablename__ = 'actor'
    id = Column(Integer, primary_key=True)
    external_actor_id = Column(Integer, unique=True)
    name = Column(String(255))
    name_eng = Column(String(255), default='')
    bio = Column(Text, default='')
    birth_day = Column(Date, nullable=True)
    death_day = Column(Date, nullable=True)
    update_date = Column(DateTime, default=datetime.datetime.utcnow)

    films = relationship("Film", secondary=Actor_Film_Role.__tablename__, viewonly=True)
    roles = relationship("Role", secondary=Actor_Film_Role.__tablename__, viewonly=True)
    fotos = relationship("Foto_Actor")

    def add_films(self, items):
        for film_id, role_id in items:
            actor_film_role_item = Actor_Film_Role.get_or_create(self.id, film_id, role_id)
            self.actor_film_roles.append(actor_film_role_item)

    @classmethod
    def get_or_create(cls, external_actor_id, name, name_eng, bio, birth_day, death_day, update_date):
        instance = cls.query.filter_by(external_actor_id=external_actor_id).first()
        if not instance:
            instance = cls(external_actor_id=external_actor_id, name=name, name_eng=name_eng,
                           bio=bio, birth_day=birth_day, death_day=death_day, update_date=update_date)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Actor {}>'.format(str(self.id) + ' ' + self.name)


class Film(Base):
    __tablename__ = 'film'
    id = Column(Integer, primary_key=True)
    original_name = Column(String(255))
    translate_name = Column(String(255))
    year = Column(Integer)
    external_film_id = Column(Integer, unique=True)
    description = Column(Text)
    update_date = Column(DateTime, default=datetime.datetime.utcnow)

    actors = relationship("Actor", secondary="actor_film_role", viewonly=True)
    roles = relationship("Role", secondary="actor_film_role", viewonly=True)
    genres = relationship("Genre", secondary="film_genre")
    fotos = relationship("Foto_Film")

    def add_actors(self, items):
        for actor, role in items:
            actor_film_role_item = Actor_Film_Role.get_or_create(actor_id, film_id, role_id)
            self.actor_film_roles.append(actor_film_role_item)

    @classmethod
    def get_or_create(cls, original_name, translate_name, year, external_film_id, description, update_date):
        instance = cls.query.filter_by(external_film_id=external_film_id).first()
        if not instance:
            instance = cls(original_name=original_name, translate_name=translate_name, year=year,
                        external_film_id=external_film_id, description=description,
                        update_date=update_date)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Film {}>'.format(self.original_name)


class Role(Base):
    __tablename__ = 'role'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))

    @classmethod
    def get_or_create(cls, name):
        instance = cls.query.filter_by(name=name).first()
        if not instance:
            instance = cls(name=name)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Role {}>'.format(self.name)


class Foto_Film(Base):

    __tablename__ = 'foto_film'

    id = Column(Integer, primary_key=True)
    foto_url = Column(String(255))
    type = Column(Enum('oboi', 'image', 'main'))

    film_id = Column(Integer, ForeignKey('film.id'))
    film = relationship("Film", backref="foto_films")

    @classmethod
    def get_or_create(cls, foto_url, type, film_id):
        instance = cls.query.filter_by(foto_url=foto_url, type=type, film_id=film_id).first()
        if not instance:
            instance = cls(foto_url=foto_url, type=type, film_id=film_id)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Foto_Film {}>'.format(self.film.original_name + " = " + self.foto_url + " - " + self.type)


class Foto_Actor(Base):

    __tablename__ = 'foto_actor'

    id = Column(Integer, primary_key=True)
    foto_url = Column(String(255))
    type = Column(Enum('oboi', 'image', 'main'))

    actor_id = Column(Integer, ForeignKey('actor.id'))
    actor = relationship("Actor", backref="foto_actors")

    @classmethod
    def get_or_create(cls, foto_url, type, actor_id):
        instance = cls.query.filter_by(foto_url=foto_url, type=type, actor_id=actor_id).first()
        if not instance:
            instance = cls(foto_url=foto_url, type=type, actor_id=actor_id)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Foto_Actor {}>'.format(self.actor.name + " = " + self.foto_url + " - " + self.type)


class Genre(Base):

    __tablename__ = 'genre'

    id = Column(Integer, primary_key=True)
    name = Column(String(255))

    @classmethod
    def get_or_create(cls, name):
        instance = cls.query.filter_by(name=name).first()
        if not instance:
            instance = cls(name=name)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __repr__(self):
        return '<Genre {}>'.format(self.name)


class Film_Genre(Base):
    __tablename__ = 'film_genre'
    genre_id = Column(Integer, ForeignKey('genre.id'), primary_key=True)
    film_id = Column(Integer, ForeignKey('film.id'), primary_key=True)

    genre = relationship("Genre", backref=backref("film_genres", cascade="all, delete-orphan" ))
    film = relationship("Film", backref=backref("film_genres", cascade="all, delete-orphan" ))

    @classmethod
    def get_or_create(cls, genre_id, film_id):
        instance = cls.query.filter_by(genre_id=genre_id, film_id=film_id).first()
        if not instance:
            instance = cls(genre_id=genre_id, film_id=film_id)
            db_session.add(instance)
            db_session.flush([instance])
        return instance

    def __init__(self, genre_id=None, film_id=None):
        self.genre_id = genre_id
        self.film_id = film_id

    def __repr__(self):
        return '<Film_Genre {}>'.format(self.film.original_name+" - "+self.genre.name)


Base.metadata.create_all(engine)


if __name__ == '__main__':

    genres = Genre.query.all()
    print(genres)

    roles = Role.query.all()
    print(roles)

    actor = Actor.query.filter_by(name="Николай Прозоровский").first()
    print(actor)

    # actor1 = Actor(name="Oreo Actor")
    # actor2 = Actor(name="Hide and Seek Actor")
    # actor3 = Actor(name="Marie Actor")
    # actor4 = Actor(name="Good Day Actor")
    #
    # db_session.add_all([actor1, actor2, actor3, actor4])
    # # db_session.commit()
    #
    # film1 = Film(original_name="First Film")
    # film2 = Film(original_name="Second Film")
    # film3 = Film(original_name="333 Film")
    # film4 = Film(original_name="444 Film")
    #
    # foto_film1 = Foto_Film(foto_url='asd', type='main', film=film1)
    #
    # genre1 = Genre.get_or_create('Genre1')
    # genre2 = Genre.get_or_create('Genre2')
    # genre3 = Genre.get_or_create('Genre3')
    #
    # film_genre1 = Film_Genre(genre=genre1, film=film3)
    # film_genre2 = Film_Genre(genre=genre2, film=film3)
    # film_genre3 = Film_Genre(genre=genre3, film=film3)
    #
    # db_session.add_all([film1, film2, foto_film1, film_genre1, film_genre2, film_genre3])
    # # db_session.commit()
    #
    # role1 = Role(name="1111First Role")
    # role2 = Role(name="2222Second Role")
    # role3 = Role(name="3333Tread Role")
    # role4 = Role(name="4444Fouth Role")
    # db_session.add_all([role1, role2, role3, role4])
    # # db_session.commit()
    #
    # role5 = Role.get_or_create("4444Fouth Role")
    # db_session.add(role5)
    # # db_session.commit()
    #
    # actor1.add_films([(film1, role1), (film1, role3), (film2, role1)])
    # actor2.add_films([(film1, role1), (film2, role2), (film3, role3), (film4, role4)])
    # actor3.add_films([(film1, role1), (film2, role2), (film3, role3), (film4, role4)])
    # actor4.add_films([(film1, role1), (film2, role2), (film3, role3), (film4, role4)])
    #
    # db_session.commit()
    #
    # print "Actor: "
    # print actor1.films[0].actor_film_roles
    #
    # print "Film: "
    # print film4.actors[0].actor_film_roles[0].actor_id
    # print film4.actors[0].actor_film_roles[0].film_id
    # print film4.actors[0].actor_film_roles[0].role_id
    #
    # print "Role: "
    # print Role.query.all()
    #
    # print "foto_film1:"
    # print foto_film1
    #
    # print "film_genre:"
    # print film3.genres
